## Slide สอน
   https://github.com/oddser/slide

## Doalod go

[download](https://golang.org/dl/) (เอาเฉพาะ go1.13.5.darwin-amd64.tar.gz)
    
    ย้าย go ไปที่ goroot path หลังจาก แตกไฟล์
    sudo mv ~/Downloads/go ~/goroot

## Set ให้ go อยู่ถาวร เวลา new terminal ใหม่
    1. cd กลับไปที่ root
    2. vi .zshrc
        export GOROOT=~/goroot
        export GOBIN=$GOPATH/bin
        export PATH=$GOBIN:$GOROOT/bin:$PATH
        
        export GOSUMDB="off"

## สร้าง src folder
    mkdir -p ~/go/src

## Go command line คราวๆ
    go env          -->         ดู evenvironment ของ go
    whick go        -->         ดูว่า go อยู่ที่ไหน
    go version      -->         check go version
    code .          -->         เปิด project go

    *** remark
        กรณี กด code . แล้วเปิด project ไม่ได้ 
        Command + ship + p      เพิ่ม            Shell Command: Install

## VS-code command line
    ctl + ~                 -->         เปิด command line ใน VS-code
    Command + ship + p      -->         เปิด Palette

## Style
    ชื่อ package ต้องเป็นตัวเล็ก
        วิธีเรียกใช้ package.Function