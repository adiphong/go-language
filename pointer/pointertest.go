package pointer

import "fmt"

func RunPointer() {
	var i int = 5
	var p *int
	p = &i
	*p = 21
	fmt.Printf("i = %+v \n", i)  // 21
	fmt.Printf("p = %+v \n", *p) // 21
}
